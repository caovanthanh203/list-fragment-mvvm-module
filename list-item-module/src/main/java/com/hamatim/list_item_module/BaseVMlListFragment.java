/*
 * Created by Cao Van Thanh on 5/29/20 6:57 PM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 5/29/20 6:57 PM
 */

package com.hamatim.list_item_module;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseVMlListFragment<VM extends ViewModel, A extends RecyclerView.Adapter> extends BaseListFragment<A> {

    protected VM viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        registerViewModel();
        registerObserver();
        return root;
    }

    protected final VM getViewModel() {
        if (viewModel == null) {
            viewModel = setViewModel();
        }
        return viewModel;
    }

    /**
     * Example:  getViewModel().getLiveData().observe(getViewLifecycleOwner(), getObserver());
     */
    protected abstract void registerObserver();

    protected abstract void registerViewModel();

    /**
     * Example: LiveViewModelProviders.of(this).get(MyViewModel.class);
     * @return
     */
    protected abstract VM setViewModel();

}
