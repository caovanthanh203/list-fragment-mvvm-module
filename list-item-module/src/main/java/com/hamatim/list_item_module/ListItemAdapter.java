package com.hamatim.list_item_module;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ListItemAdapter<M extends Item, V extends ItemViewHolder> extends RecyclerView.Adapter<V> {

    private List<M> data = new ArrayList<>();

    @NonNull
    @Override
    public V onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(getItemLayoutResId(), parent, false);
        return createViewHolderInstance(view);
    }

    protected V createViewHolderInstance(View view) {
        return (V) new ItemViewHolder(view);
    }

    protected int getItemLayoutResId() {
        return R.layout.list_item_layout;
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
        holder.setTextView(getData().get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public List<M> getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

}