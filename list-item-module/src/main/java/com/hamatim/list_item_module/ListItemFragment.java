/*
 * Created by Cao Van Thanh on 5/29/20 3:09 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 5/29/20 3:09 AM
 */

package com.hamatim.list_item_module;

import androidx.lifecycle.Observer;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/**
 * This is document
 */
public class ListItemFragment extends Fragment {

    private ListItemViewModel itemViewModel;
    private RecyclerView recyclerView;
    private Observer observer;
    private ListItemAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getContainerLayoutResId(), container, false);
        initView(root);
        initVM();
        registerObserver();
        if (savedInstanceState == null){
            getData();
        }
        return root;
    }

    private void initVM() {
        getViewModel().init();
    }

    final protected ListItemViewModel getViewModel() {
        if (itemViewModel == null){
            itemViewModel = createViewModelInstance();
        }
        return itemViewModel;
    }

    private ListItemAdapter getAdapter() {
        if (adapter == null) {
            adapter = createAdapterInstance();
        }
        return adapter;
    }

    private Observer getObserver() {
        if (observer == null){
            observer = createObserverInstance();
        }
        return observer;
    }

    private ListItemViewModel createViewModelInstance(){
        return (ListItemViewModel) ViewModelProviders.of(this).get(getViewModelClass());
    }

    //define what viewmodel will be used in this fragment
    protected Class getViewModelClass() {
        return ListItemViewModel.class;
    }

    protected void initView(View root){
        recyclerView = root.findViewById(getRecyclerListResId());
        recyclerView.setLayoutManager(getRecycerListLayoutManager());
        recyclerView.setAdapter(getAdapter());
    }

    protected ListItemAdapter createAdapterInstance() {
        return new ListItemAdapter();
    }

    protected RecyclerView.LayoutManager getRecycerListLayoutManager() {
        return new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
    }

    protected int getContainerLayoutResId() {
        return R.layout.list_item_fragment;
    }

    protected int getRecyclerListResId() {
        return R.id.listView;
    }

    protected void getData(){
        getViewModel().getData();
    }

    protected void registerObserver() {
        getViewModel().getLiveData().observe(getViewLifecycleOwner(), getObserver());
    }

    protected Observer createObserverInstance() {
        return new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> list) {
                System.out.println("Detected VM changed in " + getContext().getClass().getSimpleName());
                if (list != null && getAdapter() != null){
                    getAdapter().setData(list);
                    getAdapter().notifyDataSetChanged();
                    System.out.println("Notify to adapter id " + getAdapter().hashCode());
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getViewModel().getLiveData().removeObserver(observer);
        itemViewModel = null;
        observer = null;
    }

}
