/*
 * Created by Cao Van Thanh on 5/29/20 6:35 PM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 5/29/20 3:12 AM
 */

package com.hamatim.list_item_module;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * This is document
 */
public abstract class BaseListFragment<A extends RecyclerView.Adapter> extends Fragment {

    private RecyclerView recyclerView;
    private A adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(setLayoutId(), container, false);
        bindView(root);
        bindAdapter();
        return root;
    }

    protected int setLayoutId() {
        return R.layout.list_item_fragment;
    }

    protected int setListViewId() {
        return R.id.listView;
    }

    protected RecyclerView.LayoutManager setLayoutManager() {
        return new LinearLayoutManager(getContext(), setListDirection(), false);
    }

    protected int setListDirection(){
        return RecyclerView.VERTICAL;
    }

    protected final A getAdapter() {
        if (adapter == null) {
            adapter = setAdapter();
        }
        return adapter;
    }

    private void bindView(View root){
        recyclerView = root.findViewById(setListViewId());
        recyclerView.setLayoutManager(setLayoutManager());
    }

    private void bindAdapter(){
        recyclerView.setAdapter(getAdapter());
    }

    protected abstract A setAdapter();

}
