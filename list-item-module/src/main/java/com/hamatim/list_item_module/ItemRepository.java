package com.hamatim.list_item_module;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

public class ItemRepository<V extends Item> {

    private MutableLiveData<List<V>> itemMutableLiveData;

    public MutableLiveData<List<V>> getStream(){
        if (itemMutableLiveData == null){
            itemMutableLiveData = new MutableLiveData<>();
        }
        return itemMutableLiveData;
    }

    public void updateStream(){
        List<V> items = new ArrayList<>();
        for (int i = 0; i < 100; i++){
            items.add((V) new Item(String.format("Item value %s", i)));
        }
        getStream().setValue(items);
    }

}
