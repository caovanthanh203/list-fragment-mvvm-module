package com.hamatim.list_item_module;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemViewHolder extends RecyclerView.ViewHolder {

    private TextView textView;
    public ImageView imageView;

    public ItemViewHolder(@NonNull View root) {
        super(root);
        bindView(root);
    }

    private void bindView(View root) {
        textView = root.findViewById(R.id.textView);
        imageView = root.findViewById(R.id.imageView);
    }

    public void setTextView(String value) {
        textView.setText(value);
    }
}
