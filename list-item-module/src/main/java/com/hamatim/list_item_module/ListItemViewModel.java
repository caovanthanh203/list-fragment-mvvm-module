package com.hamatim.list_item_module;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class ListItemViewModel<M extends Item, R extends ItemRepository> extends ViewModel {

    private MutableLiveData<List<M>> itemMutableLiveData;
    private R itemRepository;

    public void init(){
        getRepository();
        itemMutableLiveData = itemRepository.getStream();
    }

    protected R createRepositoryInstance() {
        return (R) new ItemRepository();
    }

    public void getData(){
        getRepository().updateStream();
    }

    public LiveData<List<M>> getLiveData() {
        return itemMutableLiveData;
    }

    public R getRepository(){
        if (itemRepository == null) {
            itemRepository = createRepositoryInstance();
        }
        return itemRepository;
    }


}
