/*
 * Created by Cao Van Thanh on 5/29/20 7:19 PM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 5/29/20 7:19 PM
 */

package com.hamatim.list_item_module;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

public class DummyListFragment extends BaseVMlListFragment<ListItemViewModel, ListItemAdapter> {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewModel().getData();
    }

    @Override
    protected void registerObserver() {
        getViewModel().getLiveData().observe(getViewLifecycleOwner(), new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> list) {
                getAdapter().setData(list);
            }
        });
    }

    @Override
    protected void registerViewModel() {
        getViewModel().init();
    }

    @Override
    protected ListItemViewModel setViewModel() {
        return ViewModelProviders.of(this).get(ListItemViewModel.class);
    }

    @Override
    protected ListItemAdapter setAdapter() {
        return new ListItemAdapter();
    }
}
