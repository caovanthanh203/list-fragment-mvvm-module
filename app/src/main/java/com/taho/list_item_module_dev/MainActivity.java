package com.taho.list_item_module_dev;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.hamatim.list_item_module.DummyListFragment;
import com.taho.list_item_module_dev.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new DummyListFragment())
                    .commitNow();
        }
    }
}
